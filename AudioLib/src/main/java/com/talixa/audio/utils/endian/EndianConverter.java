package com.talixa.audio.utils.endian;

/**
 * This class will allow for conversion between 
 * big-endian and little-endian data formats.
 * Data stored on X86 machines is typically
 * in little-endian formt.  This means that the
 * smallest portion of a number comes first in
 * memory.  IE the number 8 stored as a short would
 * be 0x0800 in little endian format.  Java and
 * Unix/Linux uses big-endian format.  Big-endian
 * format places the high-order bytes first.  So,
 * the number 8 would be 0x0008 in big-endian.
 * The functions in this class are necessary when
 * parsing files created on an X86 machine that
 * have numbers stored in them in native format.
 * 
 * @author tgerlach
 */
public class EndianConverter {

	/**
	 * Converts a little-endian short to a Java int
	 * 
	 * Little-endian short 0x0800 00 = hiByte, 08 = lowByte
	 * Java int  		   0x0008
	 * 
	 * @param hiByte hi-order byte
	 * @param loByte lo-order byte
	 * @return java int corrosponding to the short passed in
	 */
	public static int littleEndianShortToJavaInt(byte hiByte, byte loByte) {
		int hiInt, loInt;
		
		hiInt = hiByte >= 0 ? (hiByte) : (hiByte + 256);
		loInt = loByte >= 0 ? (loByte) : (loByte + 256);						
		
		return (hiInt * 256) + loInt;
	}
	
	/**
	 * Converts a little-endian short to a Java int
	 * 
	 * @param data little endian short
	 * @return java int corrosponding to the short passed in
	 */
	public static int littleEndianShortToJavaInt(byte[] data) {
		if (data.length != 2) {
			throw new EndianConversionError("Not enough data to convert");
		}
		return littleEndianShortToJavaInt(data[1], data[0]);
	}
	
	/**
	 * Converts a little endian integer to a Java long
	 * 
	 * little-endian int 0x01020304 
	 * Java int 		 0x04030201
	 * 
	 * @param b1 highest byte
	 * @param b2 second highest byte
	 * @param b3 third highest byte
	 * @param b4 lowest byte
	 * @return java long corrosponding to the int passed in
	 */
	public static long littleEndianIntToJavaLong(byte b1, byte b2, byte b3, byte b4) {
		long l1,l2,l3,l4;
		
		l1 = b1 >= 0 ? (b1) : (b1 + 256);
		l2 = b2 >= 0 ? (b2) : (b2 + 256);
		l3 = b3 >= 0 ? (b3) : (b3 + 256);
		l4 = b4 >= 0 ? (b4) : (b4 + 256);
				
		return (l1 * 256*256*256) + (l2 * 256*256) + (l3 * 256) + (l4);
	}
	
	/**
	 * Converts a little-endian int to a Java long
	 * 
	 * @param data little endian int
	 * @return java long corrosponding to the int passed in
	 */
	public static long littleEndianIntToJavaLong(byte[] data) {
		if (data.length != 4) {
			throw new EndianConversionError("Not enough data to convert");
		}
		return littleEndianIntToJavaLong(data[3],data[2],data[1],data[0]);
	}
	
	/**
	 * Converts a java short to a series of big-endian bytes
	 * 
	 * @param javaShort short to convert
	 * @return bytes of short in big-endian order
	 */
	public static byte[] javaShortToBigEndianBytes(short javaShort) {
		byte[] data = new byte[2];
		data[0] = (byte)(javaShort & 0xFF);
		data[1] = (byte)((javaShort >> 8) & 0xFF);
		return data;
	}
	
	/**
	 * Converts a java int to a series of big-endian bytes
	 * 
	 * @param javaInt short to convert
	 * @return bytes of int in big-endian order
	 */
	public static byte[] javaIntToBigEndianBytes(int javaInt) {
		byte[] data = new byte[4];
		data[0] = (byte)(javaInt & 0xFF);
		data[1] = (byte)((javaInt >> 8) & 0xFF);
		data[2] = (byte)((javaInt >> 16) & 0xFF);
		data[3] = (byte)((javaInt >> 24) & 0xFF);
		return data;
	}
}
