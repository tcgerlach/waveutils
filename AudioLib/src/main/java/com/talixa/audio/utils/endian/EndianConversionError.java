package com.talixa.audio.utils.endian;

/**
 * This class represents an error in the endian conversion
 * process.  This typically happens when trying to convert 
 * a group of bytes back to a java integral type when
 * there are not enough bytes to fill the integer type.
 * 
 * @author tgerlach
 */
@SuppressWarnings("serial")
public class EndianConversionError extends RuntimeException {
	public EndianConversionError(String msg) {
		super(msg);
	}
}