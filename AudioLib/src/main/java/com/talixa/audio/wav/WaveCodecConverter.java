package com.talixa.audio.wav;

import java.io.File;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;

public class WaveCodecConverter {
	
	public static void main(String args[]) {
		if (args.length != 3) {
			System.out.println("Call is WaveCoderConverter -f infile outfile");
			System.out.println("where f is m for mulaw, a for alaw, or p for pcm");
		}
		
		String outputFormat = args[0];
		String inputFile    = args[1];
		String outputFile   = args[2];
		
		if (outputFormat.equalsIgnoreCase("-m")) {
			convertToMulaw(inputFile, outputFile);
		} else if (outputFormat.equalsIgnoreCase("-a")) {
			convertToAlaw(inputFile, outputFile);
		} else if (outputFormat.equalsIgnoreCase("-p")) {
			convertToPcm(inputFile, outputFile);
		}			
	}
	
	public static final AudioFormat MULAW_8K 	= new AudioFormat(new AudioFormat.Encoding("ULAW"), 		8000, 8, 1, 1, 8000, true);
	public static final AudioFormat PCM_16BIT   = new AudioFormat(new AudioFormat.Encoding("PCM_SIGNED"), 	8000, 16, 1, 2, 8000, false);
	public static final AudioFormat PCM_8BIT   	= new AudioFormat(new AudioFormat.Encoding("PCM_UNSIGNED"), 8000, 8, 1, 1, 8000, false);
	public static final AudioFormat ALAW_8K  	= new AudioFormat(new AudioFormat.Encoding("ALAW"), 		8000, 8, 1, 1, 8000, false);
	
	public static void convertToMulaw(String infile, String outfile) {		
		convert(infile,outfile,MULAW_8K);		
	}
	
	public static void convertToPcm(String infile, String outfile) {	
		convert(infile,outfile,PCM_16BIT);	
	}
	
	public static void convertToAlaw(String infile, String outfile) {	
		convert(infile,outfile,ALAW_8K);			
	}
	
	public static void convert(String infile, String outfile, AudioFormat targetFormat) {
		try {			
			// Create files
			File sourceFile = new File(infile);		 
			File targetFile = new File(outfile);	
			
			// Create streams
			AudioInputStream sourceAudioInputStream = AudioSystem.getAudioInputStream(sourceFile);		 		
			AudioInputStream targetAudioInputStream = AudioSystem.getAudioInputStream(targetFormat, sourceAudioInputStream);			
			
			// Write output
			AudioSystem.write(targetAudioInputStream, AudioFileFormat.Type.WAVE, targetFile);
			
			// Close streams
			sourceAudioInputStream.close();
			targetAudioInputStream.close();						
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
