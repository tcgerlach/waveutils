package com.talixa.audio.wav.exceptions;

import com.talixa.audio.riff.exceptions.RiffFormatException;

/**
 * @author tgerlach
 */
@SuppressWarnings("serial")
public class WaveFormatException extends RiffFormatException {
	public WaveFormatException(final String msg) {
		super(msg);
	}	
}
