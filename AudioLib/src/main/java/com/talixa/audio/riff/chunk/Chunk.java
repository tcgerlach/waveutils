package com.talixa.audio.riff.chunk;

import java.io.Serializable;

/**
 * A generic RIFF chunk
 * 
 * @author tgerlach
 */
@SuppressWarnings("serial")
public abstract class Chunk implements Serializable{

	/**
	 * Returns the chunk header
	 * 
	 * @return chunk header
	 */
	abstract public byte[] getChunkHeader();
	
	/**
	 * Returns the length of the chunk data
	 * 
	 * @return
	 */
	abstract public int getChunkLength();
	
	/**
	 * Returns the length of the entire chunk, including header
	 * 
	 * @return length of entire chunk
	 */
	abstract public int getByteLength();
	
	/**
	 * Returns the chunk data
	 * 
	 * @return chunk data
	 */
	abstract public byte[] getChunkData();
	
	/**
	 * Set the chunk data from a byte array
	 * 
	 * @param chunkData data
	 * @throws ChunkFormatException
	 */
	abstract public void setChunkData(byte[] chunkData) throws ChunkFormatException;
	
	/**
	 * Returns the entire chunk as bytes, including the header
	 * 
	 * @return entire chunk in bytes
	 */
	abstract public byte[] getBytes();	
	
	public String toString() {
		String chunkString = new String("0x");
		byte[] bytes = getBytes();
		for(int i = 0; i < bytes.length; ++i) {
			String hexDigit = Integer.toHexString(bytes[i] < 0 ? bytes[i] + 256: bytes[i]);
			chunkString += hexDigit.length() == 2 ? hexDigit : "0" + hexDigit;
		}
		return chunkString;
	}
}
