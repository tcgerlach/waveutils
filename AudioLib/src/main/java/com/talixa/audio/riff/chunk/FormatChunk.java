package com.talixa.audio.riff.chunk;

import com.talixa.audio.utils.endian.EndianConverter;

/**
 * The format chunk contains information
 * about the format of the audio data stored
 * in the data chunk
 * 
 * @author tgerlach
 */
@SuppressWarnings("serial")
public class FormatChunk extends Chunk {

	// Chunk header information
	private static final int FORMAT_CHUNK_SIZE = 0x10;
	
	/*
	 * Some wave files have a 'fact' chunk.  It appears that when
	 * a fact chunk is present, the format chunk is two bytes larger
	 * but, when it is larger, the last two bytes are 0x00
	 */
	private static final int FORMAT_CHUNK_SIZE_W_FACT_CHUNK = FORMAT_CHUNK_SIZE + 2;
	
	public static final byte[] FORMAT_CHUNK_HEADER = {'f', 'm', 't', ' '};

	// Chunk data field values
	public static final short AUDIO_FORMAT_PCM    			= 0x0001;
	public static final short AUDIO_FORMAT_MICROSOFT_ADPCM 		= 0x0002;
	public static final short AUDIO_FORMAT_A_LAW  			= 0x0006;
	public static final short AUDIO_FORMAT_MU_LAW 			= 0x0007;
	public static final short AUDIO_FORMAT_IMA_ADPCM 		= 0x0011;
	public static final short AUDIO_FORMAT_G723_ADPCM		= 0x0016;
	public static final short AUDIO_FORMAT_GSM_610			= 0x0031;
	public static final short AUDIO_FORMAT_G721_ADPCM		= 0x0040;
	public static final short AUDIO_FORMAT_MPEG			= 0x0050;

	public static final short NUM_CHANNELS_1		= 0x0001;
	public static final short NUM_CHANNELS_2		= 0x0002;

	public static final int   SAMPLE_RATE_8K		= 0x00001F40;
	public static final int   SAMPLE_RATE_7K		= 0x00001B58;

	public static final short BITS_PER_SAMPLE_8	= 0x0008;
	public static final short BITS_PER_SAMPLE_16  = 0x0010;

	private static final short BITS_PER_CHANNEL 	= 8;

	// data fields
	private short audioFormat;
	private short numChannels;
	private int sampleRate;
	private short bitsPerSample;
	private int byteRate;
	private short blockAlign;

	public FormatChunk() {
	}

	public short getAudioFormat() {
		return audioFormat;
	}

	public void setAudioFormat(short audioFormat) {
		this.audioFormat = audioFormat;
	}

	public short getBitsPerSample() {
		return bitsPerSample;
	}

	public void setBitsPerSample(short bitsPerSample) {
		this.bitsPerSample = bitsPerSample;
		byteRate = sampleRate * numChannels * bitsPerSample/8;
	}

	public short getNumChannels() {
		return numChannels;
	}

	public void setNumChannels(short numChannels) {
		this.numChannels = numChannels;
		byteRate = sampleRate * numChannels * bitsPerSample/8;
		blockAlign = (short)(numChannels * (BITS_PER_CHANNEL/8));
	}

	public int getSampleRate() {
		return sampleRate;
	}

	public void setSampleRate(int sampleRate) {
		this.sampleRate = sampleRate;
		byteRate = sampleRate * numChannels * bitsPerSample/8;
	}

	public byte[] getChunkHeader() {
		return FORMAT_CHUNK_HEADER;
	}

	public int getByteLength() {
		return FORMAT_CHUNK_SIZE + 8; // +4 for header & +4 for length
	}

	public byte[] getChunkData() {
		byte[] chunkData = new byte[FORMAT_CHUNK_SIZE];
		System.arraycopy(EndianConverter.javaShortToBigEndianBytes(audioFormat), 	0, chunkData,  0, 2);
		System.arraycopy(EndianConverter.javaShortToBigEndianBytes(numChannels), 	0, chunkData,  2, 2);
		System.arraycopy(EndianConverter.javaIntToBigEndianBytes(sampleRate), 		0, chunkData,  4, 4);
		System.arraycopy(EndianConverter.javaIntToBigEndianBytes(byteRate), 		0, chunkData,  8, 4);
		System.arraycopy(EndianConverter.javaShortToBigEndianBytes(blockAlign), 	0, chunkData, 12, 2);	
		System.arraycopy(EndianConverter.javaShortToBigEndianBytes(bitsPerSample), 	0, chunkData, 14, 2);
		return chunkData;
	}

	public void setChunkData(byte[] chunkData) throws ChunkFormatException {
		if (chunkData.length != FORMAT_CHUNK_SIZE && chunkData.length != FORMAT_CHUNK_SIZE_W_FACT_CHUNK) {
			throw new ChunkFormatException("Bad format chunk size: " + chunkData.length);
		}
		audioFormat 	= (short)EndianConverter.littleEndianShortToJavaInt(chunkData[1], chunkData[0]);	
		numChannels 	= (short)EndianConverter.littleEndianShortToJavaInt(chunkData[3], chunkData[2]);
		sampleRate		= (int)EndianConverter.littleEndianIntToJavaLong(chunkData[7], chunkData[6], chunkData[5], chunkData[4]);
		byteRate		= (int)EndianConverter.littleEndianIntToJavaLong(chunkData[11], chunkData[10], chunkData[9], chunkData[8]);
		blockAlign		= (short)EndianConverter.littleEndianShortToJavaInt(chunkData[13], chunkData[12]);
		bitsPerSample	= (short)EndianConverter.littleEndianShortToJavaInt(chunkData[15], chunkData[14]);
	}

	public byte[] getBytes() {
		byte[] chunkBytes = new byte[getByteLength()];
		System.arraycopy(FORMAT_CHUNK_HEADER, 0, chunkBytes, 0, 4);
		System.arraycopy(EndianConverter.javaIntToBigEndianBytes(FORMAT_CHUNK_SIZE), 0, chunkBytes, 4, 4);
		System.arraycopy(getChunkData(), 0, chunkBytes, 8, FORMAT_CHUNK_SIZE);
		return chunkBytes;
	}

	public int getChunkLength() {
		return FORMAT_CHUNK_SIZE; 
	}
}
