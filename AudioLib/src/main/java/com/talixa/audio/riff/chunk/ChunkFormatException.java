package com.talixa.audio.riff.chunk;

/**
 * Exception for any error in the formatting of a chunk
 * 
 * @author tgerlach
 */
@SuppressWarnings("serial")
public class ChunkFormatException extends Exception {

	public ChunkFormatException() {
	}
	
	public ChunkFormatException(String string) {
		super(string);
	}	
}
