package com.talixa.audio.riff.exceptions;

/**
 * @author tgerlach
 */
@SuppressWarnings("serial")
public class RiffFormatException extends Exception {
	public RiffFormatException(final String msg) {
		super(msg);
	}
}
