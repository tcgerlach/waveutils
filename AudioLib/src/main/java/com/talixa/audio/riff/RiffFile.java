package com.talixa.audio.riff;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * @author tgerlach
 */
public abstract class RiffFile {

	public static final byte[] RIFF_HEADER = {'R', 'I', 'F', 'F'};
	
	public abstract byte[] getRiffHeader();
	public abstract int getRiffDataSize();
	public abstract byte[] getRiffDataType();
	public abstract byte[] getBytes();
	
	public String toString() {
		String chunkString = new String("0x");
		byte[] bytes = getBytes();
		for(int i = 0; i < bytes.length; ++i) {
			String hexDigit = Integer.toHexString(bytes[i] < 0 ? bytes[i] + 256: bytes[i]);
			chunkString += hexDigit.length() == 2 ? hexDigit : "0" + hexDigit;
		}
		return chunkString;
	}
	
	/**
	 * Function to write this riff file to the filesystem
	 * 
	 * @param filename output filename
	 * @throws IOException
	 */
	public void outputToFile(String filename) throws IOException {
		File file = new File(filename);		
		OutputStream os = new FileOutputStream(file);
		try {
			os.write(getBytes());
		} finally {
			os.close();
		}
	}
}
