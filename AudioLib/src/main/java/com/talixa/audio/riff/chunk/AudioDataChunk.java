package com.talixa.audio.riff.chunk;

import com.talixa.audio.utils.endian.EndianConverter;


/**
 * An audio chunk consists of a header containing 'data' and a 
 * 4 byte little endian data size followed the audio data
 * 
 * @author tgerlach
 */
@SuppressWarnings("serial")
public class AudioDataChunk extends Chunk {

	public static final byte[] AUDIO_DATA_CHUNK_HEADER = {'d', 'a', 't', 'a'};
	private byte[] data;
	
	public byte[] getChunkHeader() {
		return AUDIO_DATA_CHUNK_HEADER;
	}

	public int getChunkLength() {
		return data.length;
	}

	public byte[] getChunkData() {
		return data;
	}

	public void setChunkData(byte[] chunkData) {
		data = chunkData;	
	}
	
	public int getAudioSampleAt(int sampleSize, int index) {
		if (sampleSize == 8) {
			return data[index];
		} else {
			return EndianConverter.littleEndianShortToJavaInt(data[index+1],data[index]);
		}
	}
	
	public byte[] getBytes() {	
		byte[] audioData = new byte[getByteLength()];
		System.arraycopy(AUDIO_DATA_CHUNK_HEADER, 0, audioData, 0, 4);
		System.arraycopy(EndianConverter.javaIntToBigEndianBytes(data.length), 0, audioData, 4, 4);
		System.arraycopy(data, 0, audioData, 8, data.length);
		return audioData;
	}

	public int getByteLength() {
		return getChunkLength() + 8; // +4 for header & +4 for data length;
	}
}
